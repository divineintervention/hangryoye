import React from 'react';
import ReactDOM from 'react-dom';
import {Provider} from 'react-redux';
import App from './modules';
import registerServiceWorker from './registerServiceWorker';
import store, {runSaga} from './store';
import 'grommet/grommet-aruba.min.css';

ReactDOM.render(
  <Provider store={store}>
    <App />
  </Provider>,
  document.getElementById('root')
);
runSaga();
registerServiceWorker();
