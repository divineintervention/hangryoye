import R from 'ramda';
import axios, {CancelToken} from 'axios';

import {BASE_URL} from '../config';

const axs = axios.create({
  baseUrl: BASE_URL
});

export const get = (url, config) =>
  axs.get(url, config);
  //   ...config,
  //   headers: {
  //     'Accept': accept,
  //     'Accept-Type': accept,
  //     'Cache-Control': 'no-cache',
  //     'Pragma': 'no-cache',
  //     ...R.path(['headers'], config)
  //   }
  // });
