const baseUrls = {
  local: 'http://localhost:4000/api',
  prod: 'https://hangryoye.co/api'
};

export default {
  BASE_URL: baseUrls.local
};
