import 'jest-enzyme'; // Provides expect matchers in tests

const localStorageMock = { // localStorage
  getItem: jest.fn(),
  setItem: jest.fn(),
  clear: jest.fn()
};
global.localStorage = localStorageMock;
