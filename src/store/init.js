import { createReducer } from './utils';

const sample = {
  bikes: [
    {
      id: 1,
      make: 'Honda',
      color: 'Black',
      model: 'Activa',
      engine: '1234',
      chassis: '123',
      rtoNumber: 'KA05J1579',
      purchasedOn: '20160810',
      lastServicedOn: '20170310',
      lastServiceCost: '900',
      nextServiceOn: '20171010',
      kilometers: '40000',
      lastMeteredOn: '20170527'
    }
  ],
  couriers: [
    {
      id: 1,
      name: 'Khalil',
      phone: '9988776655',
      isActive: true
    }
  ],
  bills: {},
  price: 100,
  address: {},
  isCancelled: false,
  cancellationReason: null
};

export const initialState = {
  incoming: sample,
  assigned: sample,
  delivered: sample
};
