import React from 'react';

import Search from 'grommet/components/Search';

export const SearchInput = () => (
  <Search
    dropAlign={{right: 'right', bottom: 'bottom'}}
    fill={true}
    iconAlign="end"
    align="center"
    inline={true}
    responsive={true}
  />
);

export default SearchInput;
