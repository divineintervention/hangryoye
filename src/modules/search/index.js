import React from 'react';

import Article from 'grommet/components/Article';
import Section from 'grommet/components/Section';

import SearchInput from './input';

export const AppSearch = () => (
  <Article>
    <Section>
      <SearchInput />
    </Section>
    <Section>
    </Section>
  </Article>
);

export default AppSearch;
