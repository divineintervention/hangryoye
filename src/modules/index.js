import React, { Component } from 'react';
import GrommetApp from 'grommet/components/App'; // App
import AppHeader from './header';
import AppFooter from './footer';
import AppSearch from './search';
import socket from '../phoenixSocket';

class App extends Component {

  componentDidMount() {
    console.log(socket);
  }

  render() {
    return (
      <GrommetApp>
        <AppHeader />
        <AppSearch />
        <AppFooter />
      </GrommetApp>
    );
  }
}

export default App;
