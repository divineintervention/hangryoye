import React from 'react';

import Footer from 'grommet/components/Footer';
import Paragraph from 'grommet/components/Paragraph';

export const AppFooter = () => (
  <Footer
    primary={true}
    justify='center'
    align='center'
    alignContent='center'
    textAlign='center'
  >
    <Paragraph align='center'>
      © 2017 Divine Intervention
    </Paragraph>
  </Footer>
);

export default AppFooter;
