import {get} from '../services/http';

export const getVendors = () => get('vendors');
