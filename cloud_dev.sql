--
-- PostgreSQL database dump
--

-- Dumped from database version 9.5.8
-- Dumped by pg_dump version 9.5.8

SET statement_timeout = 0;
SET lock_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SET check_function_bodies = false;
SET client_min_messages = warning;
SET row_security = off;

--
-- Name: plpgsql; Type: EXTENSION; Schema: -; Owner: 
--

CREATE EXTENSION IF NOT EXISTS plpgsql WITH SCHEMA pg_catalog;


--
-- Name: EXTENSION plpgsql; Type: COMMENT; Schema: -; Owner: 
--

COMMENT ON EXTENSION plpgsql IS 'PL/pgSQL procedural language';


SET search_path = public, pg_catalog;

SET default_tablespace = '';

SET default_with_oids = false;

--
-- Name: addresses; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE addresses (
    id integer NOT NULL,
    label character varying(255),
    street character varying(255),
    landmark character varying(255),
    city character varying(255),
    pincode character varying(255),
    latitude double precision,
    longitude double precision,
    user_id integer,
    inserted_at timestamp without time zone NOT NULL,
    updated_at timestamp without time zone NOT NULL
);


ALTER TABLE addresses OWNER TO postgres;

--
-- Name: addresses_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE addresses_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE addresses_id_seq OWNER TO postgres;

--
-- Name: addresses_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE addresses_id_seq OWNED BY addresses.id;


--
-- Name: billitems; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE billitems (
    id integer NOT NULL,
    quantity integer,
    bill_id integer,
    menu_id integer,
    inserted_at timestamp without time zone NOT NULL,
    updated_at timestamp without time zone NOT NULL
);


ALTER TABLE billitems OWNER TO postgres;

--
-- Name: billitems_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE billitems_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE billitems_id_seq OWNER TO postgres;

--
-- Name: billitems_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE billitems_id_seq OWNED BY billitems.id;


--
-- Name: bills; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE bills (
    id integer NOT NULL,
    is_collected boolean DEFAULT false NOT NULL,
    vendor_id integer,
    delivery_id integer,
    inserted_at timestamp without time zone NOT NULL,
    updated_at timestamp without time zone NOT NULL
);


ALTER TABLE bills OWNER TO postgres;

--
-- Name: bills_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE bills_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE bills_id_seq OWNER TO postgres;

--
-- Name: bills_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE bills_id_seq OWNED BY bills.id;


--
-- Name: couriers; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE couriers (
    id integer NOT NULL,
    name character varying(255),
    phone character varying(255),
    is_active boolean DEFAULT false NOT NULL,
    inserted_at timestamp without time zone NOT NULL,
    updated_at timestamp without time zone NOT NULL
);


ALTER TABLE couriers OWNER TO postgres;

--
-- Name: couriers_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE couriers_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE couriers_id_seq OWNER TO postgres;

--
-- Name: couriers_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE couriers_id_seq OWNED BY couriers.id;


--
-- Name: cuisines; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE cuisines (
    id integer NOT NULL,
    name character varying(255),
    inserted_at timestamp without time zone NOT NULL,
    updated_at timestamp without time zone NOT NULL
);


ALTER TABLE cuisines OWNER TO postgres;

--
-- Name: cuisines_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE cuisines_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE cuisines_id_seq OWNER TO postgres;

--
-- Name: cuisines_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE cuisines_id_seq OWNED BY cuisines.id;


--
-- Name: deliveries; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE deliveries (
    id integer NOT NULL,
    is_complete boolean DEFAULT false NOT NULL,
    courier_id integer,
    user_id integer,
    address_id integer,
    inserted_at timestamp without time zone NOT NULL,
    updated_at timestamp without time zone NOT NULL
);


ALTER TABLE deliveries OWNER TO postgres;

--
-- Name: deliveries_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE deliveries_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE deliveries_id_seq OWNER TO postgres;

--
-- Name: deliveries_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE deliveries_id_seq OWNED BY deliveries.id;


--
-- Name: menus; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE menus (
    id integer NOT NULL,
    name character varying(255),
    price integer,
    is_available boolean DEFAULT false NOT NULL,
    category character varying(255),
    description character varying(255),
    ingredients character varying(255)[],
    contains_egg boolean DEFAULT false NOT NULL,
    contains_meat boolean DEFAULT false NOT NULL,
    vendor_id integer,
    cuisine_id integer,
    inserted_at timestamp without time zone NOT NULL,
    updated_at timestamp without time zone NOT NULL
);


ALTER TABLE menus OWNER TO postgres;

--
-- Name: menus_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE menus_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE menus_id_seq OWNER TO postgres;

--
-- Name: menus_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE menus_id_seq OWNED BY menus.id;


--
-- Name: schema_migrations; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE schema_migrations (
    version bigint NOT NULL,
    inserted_at timestamp without time zone
);


ALTER TABLE schema_migrations OWNER TO postgres;

--
-- Name: users; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE users (
    id integer NOT NULL,
    token character varying(255),
    name character varying(255),
    phone character varying(255),
    otp character varying(255),
    is_verified boolean DEFAULT false NOT NULL,
    inserted_at timestamp without time zone NOT NULL,
    updated_at timestamp without time zone NOT NULL
);


ALTER TABLE users OWNER TO postgres;

--
-- Name: users_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE users_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE users_id_seq OWNER TO postgres;

--
-- Name: users_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE users_id_seq OWNED BY users.id;


--
-- Name: vendors; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE vendors (
    id integer NOT NULL,
    is_open boolean DEFAULT false NOT NULL,
    name character varying(255),
    locality character varying(255),
    address character varying(255),
    opens_at time without time zone,
    closes_at time without time zone,
    token character varying(255),
    phone character varying(255),
    otp character varying(255),
    is_verified boolean DEFAULT false NOT NULL,
    latitude double precision,
    longitude double precision,
    inserted_at timestamp without time zone NOT NULL,
    updated_at timestamp without time zone NOT NULL
);


ALTER TABLE vendors OWNER TO postgres;

--
-- Name: vendors_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE vendors_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE vendors_id_seq OWNER TO postgres;

--
-- Name: vendors_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE vendors_id_seq OWNED BY vendors.id;


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY addresses ALTER COLUMN id SET DEFAULT nextval('addresses_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY billitems ALTER COLUMN id SET DEFAULT nextval('billitems_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY bills ALTER COLUMN id SET DEFAULT nextval('bills_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY couriers ALTER COLUMN id SET DEFAULT nextval('couriers_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY cuisines ALTER COLUMN id SET DEFAULT nextval('cuisines_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY deliveries ALTER COLUMN id SET DEFAULT nextval('deliveries_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY menus ALTER COLUMN id SET DEFAULT nextval('menus_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY users ALTER COLUMN id SET DEFAULT nextval('users_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY vendors ALTER COLUMN id SET DEFAULT nextval('vendors_id_seq'::regclass);


--
-- Data for Name: addresses; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY addresses (id, label, street, landmark, city, pincode, latitude, longitude, user_id, inserted_at, updated_at) FROM stdin;
\.


--
-- Name: addresses_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('addresses_id_seq', 1, false);


--
-- Data for Name: billitems; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY billitems (id, quantity, bill_id, menu_id, inserted_at, updated_at) FROM stdin;
\.


--
-- Name: billitems_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('billitems_id_seq', 1, false);


--
-- Data for Name: bills; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY bills (id, is_collected, vendor_id, delivery_id, inserted_at, updated_at) FROM stdin;
\.


--
-- Name: bills_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('bills_id_seq', 1, false);


--
-- Data for Name: couriers; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY couriers (id, name, phone, is_active, inserted_at, updated_at) FROM stdin;
1	Ram	9999999999	f	2017-07-30 18:48:43.475957	2017-07-30 18:48:43.475962
2	Shyam	8888888888	f	2017-07-30 18:48:43.480823	2017-07-30 18:48:43.480829
\.


--
-- Name: couriers_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('couriers_id_seq', 2, true);


--
-- Data for Name: cuisines; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY cuisines (id, name, inserted_at, updated_at) FROM stdin;
1	Indian	2017-07-30 18:48:43.430888	2017-07-30 18:48:43.432587
2	Chinese	2017-07-30 18:48:43.470027	2017-07-30 18:48:43.470032
\.


--
-- Name: cuisines_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('cuisines_id_seq', 2, true);


--
-- Data for Name: deliveries; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY deliveries (id, is_complete, courier_id, user_id, address_id, inserted_at, updated_at) FROM stdin;
\.


--
-- Name: deliveries_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('deliveries_id_seq', 1, false);


--
-- Data for Name: menus; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY menus (id, name, price, is_available, category, description, ingredients, contains_egg, contains_meat, vendor_id, cuisine_id, inserted_at, updated_at) FROM stdin;
1	Chicken Biryani	100	t	Rice	Spicy chicken cooked in tender rice	{Chicken,Rice,Spices}	t	t	1	1	2017-07-30 18:48:43.497533	2017-07-30 18:48:43.497555
2	Chicken Shawarma	100	t	Rolls	Shredded, roasted, tender chicken wrapped in a pita with a herbed-mayo spread	{Chicken,Pita,Mayo}	t	t	1	1	2017-07-30 18:48:43.506846	2017-07-30 18:48:43.506852
3	Plain paratha	50	t	Breads	The usual paratha with no surprises	{Wheatflour,Ajwain,Salt,Spices}	f	f	2	1	2017-07-30 18:48:43.51787	2017-07-30 18:48:43.517876
4	Kadhai paneer	110	t	Curry	Paneer and vegetables cooked in a 'Kadhai' curry with spices	{Paneer,Vegetables,Salt,Spices}	f	f	2	1	2017-07-30 18:48:43.52882	2017-07-30 18:48:43.528827
\.


--
-- Name: menus_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('menus_id_seq', 4, true);


--
-- Data for Name: schema_migrations; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY schema_migrations (version, inserted_at) FROM stdin;
20170724192723	2017-07-30 18:47:21.650913
20170724200120	2017-07-30 18:47:21.703079
20170724200817	2017-07-30 18:47:21.742524
20170724201909	2017-07-30 18:47:21.776768
20170724202130	2017-07-30 18:47:21.802586
20170724202618	2017-07-30 18:47:21.846696
20170724204022	2017-07-30 18:47:21.897028
20170724204507	2017-07-30 18:47:21.938799
20170724204838	2017-07-30 18:47:21.977629
\.


--
-- Data for Name: users; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY users (id, token, name, phone, otp, is_verified, inserted_at, updated_at) FROM stdin;
\.


--
-- Name: users_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('users_id_seq', 1, false);


--
-- Data for Name: vendors; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY vendors (id, is_open, name, locality, address, opens_at, closes_at, token, phone, otp, is_verified, latitude, longitude, inserted_at, updated_at) FROM stdin;
1	t	Zuber Biryaniwala	Kaktives Road	Kaktives Road	10:00:00	18:00:00	token	7777777777	123456	t	15.8646329999999995	74.5115420000000057	2017-07-30 18:48:43.485901	2017-07-30 18:48:43.485908
2	t	Paratha Corner	Khanapur Road	Khanapur Road	10:00:00	18:00:00	token	6666666666	123456	t	15.8294730000000001	74.4995710000000031	2017-07-30 18:48:43.491879	2017-07-30 18:48:43.491884
\.


--
-- Name: vendors_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('vendors_id_seq', 2, true);


--
-- Name: addresses_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY addresses
    ADD CONSTRAINT addresses_pkey PRIMARY KEY (id);


--
-- Name: billitems_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY billitems
    ADD CONSTRAINT billitems_pkey PRIMARY KEY (id);


--
-- Name: bills_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY bills
    ADD CONSTRAINT bills_pkey PRIMARY KEY (id);


--
-- Name: couriers_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY couriers
    ADD CONSTRAINT couriers_pkey PRIMARY KEY (id);


--
-- Name: cuisines_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY cuisines
    ADD CONSTRAINT cuisines_pkey PRIMARY KEY (id);


--
-- Name: deliveries_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY deliveries
    ADD CONSTRAINT deliveries_pkey PRIMARY KEY (id);


--
-- Name: menus_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY menus
    ADD CONSTRAINT menus_pkey PRIMARY KEY (id);


--
-- Name: schema_migrations_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY schema_migrations
    ADD CONSTRAINT schema_migrations_pkey PRIMARY KEY (version);


--
-- Name: users_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY users
    ADD CONSTRAINT users_pkey PRIMARY KEY (id);


--
-- Name: vendors_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY vendors
    ADD CONSTRAINT vendors_pkey PRIMARY KEY (id);


--
-- Name: addresses_user_id_index; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX addresses_user_id_index ON addresses USING btree (user_id);


--
-- Name: billitems_bill_id_index; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX billitems_bill_id_index ON billitems USING btree (bill_id);


--
-- Name: billitems_menu_id_index; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX billitems_menu_id_index ON billitems USING btree (menu_id);


--
-- Name: bills_delivery_id_index; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX bills_delivery_id_index ON bills USING btree (delivery_id);


--
-- Name: bills_vendor_id_index; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX bills_vendor_id_index ON bills USING btree (vendor_id);


--
-- Name: couriers_phone_index; Type: INDEX; Schema: public; Owner: postgres
--

CREATE UNIQUE INDEX couriers_phone_index ON couriers USING btree (phone);


--
-- Name: deliveries_address_id_index; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX deliveries_address_id_index ON deliveries USING btree (address_id);


--
-- Name: deliveries_courier_id_index; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX deliveries_courier_id_index ON deliveries USING btree (courier_id);


--
-- Name: deliveries_user_id_index; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX deliveries_user_id_index ON deliveries USING btree (user_id);


--
-- Name: menus_cuisine_id_index; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX menus_cuisine_id_index ON menus USING btree (cuisine_id);


--
-- Name: menus_vendor_id_index; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX menus_vendor_id_index ON menus USING btree (vendor_id);


--
-- Name: users_phone_index; Type: INDEX; Schema: public; Owner: postgres
--

CREATE UNIQUE INDEX users_phone_index ON users USING btree (phone);


--
-- Name: addresses_user_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY addresses
    ADD CONSTRAINT addresses_user_id_fkey FOREIGN KEY (user_id) REFERENCES users(id);


--
-- Name: billitems_bill_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY billitems
    ADD CONSTRAINT billitems_bill_id_fkey FOREIGN KEY (bill_id) REFERENCES bills(id);


--
-- Name: billitems_menu_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY billitems
    ADD CONSTRAINT billitems_menu_id_fkey FOREIGN KEY (menu_id) REFERENCES menus(id);


--
-- Name: bills_delivery_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY bills
    ADD CONSTRAINT bills_delivery_id_fkey FOREIGN KEY (delivery_id) REFERENCES deliveries(id);


--
-- Name: bills_vendor_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY bills
    ADD CONSTRAINT bills_vendor_id_fkey FOREIGN KEY (vendor_id) REFERENCES vendors(id);


--
-- Name: deliveries_address_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY deliveries
    ADD CONSTRAINT deliveries_address_id_fkey FOREIGN KEY (address_id) REFERENCES addresses(id);


--
-- Name: deliveries_courier_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY deliveries
    ADD CONSTRAINT deliveries_courier_id_fkey FOREIGN KEY (courier_id) REFERENCES couriers(id);


--
-- Name: deliveries_user_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY deliveries
    ADD CONSTRAINT deliveries_user_id_fkey FOREIGN KEY (user_id) REFERENCES users(id);


--
-- Name: menus_cuisine_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY menus
    ADD CONSTRAINT menus_cuisine_id_fkey FOREIGN KEY (cuisine_id) REFERENCES cuisines(id);


--
-- Name: menus_vendor_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY menus
    ADD CONSTRAINT menus_vendor_id_fkey FOREIGN KEY (vendor_id) REFERENCES vendors(id);


--
-- Name: public; Type: ACL; Schema: -; Owner: postgres
--

REVOKE ALL ON SCHEMA public FROM PUBLIC;
REVOKE ALL ON SCHEMA public FROM postgres;
GRANT ALL ON SCHEMA public TO postgres;
GRANT ALL ON SCHEMA public TO PUBLIC;


--
-- PostgreSQL database dump complete
--

